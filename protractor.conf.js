(function () {
    'use strict';

    var grunt = require('grunt'),
        path = require('path'),
        allScriptsTimeout = 11000,
        args = require('optimist').argv;

    exports.config = {
        allScriptsTimeout: allScriptsTimeout,
        framework: 'jasmine2',
        keepAlive: true,

        multiCapabilities: [
            {
                'browserName': 'chrome',
                'chromeOptions': {
                    args: ['test-type', '--start-maximized'] // get rid of the ignore cert warning
                },
                shardTestFiles: true,
                maxInstances: 10
            }
        ],

        params: {},

        onPrepare: function () {
            // Disable animations so e2e tests run more quickly
            var disableNgAnimate = function() {
                angular.module('disableNgAnimate', []).run(['$animate', function($animate) {
                    $animate.enabled(false);
                }]);
            };

            browser.addMockModule('disableNgAnimate', disableNgAnimate);

            var disableCssAnimate = function() {
                angular
                    .module('disableCssAnimate', [])
                    .run(function() {
                        var style = document.createElement('style');
                        style.type = 'text/css';
                        style.innerHTML = '* {' +
                            '-webkit-transition: none !important;' +
                            '-moz-transition: none !important' +
                            '-o-transition: none !important' +
                            '-ms-transition: none !important' +
                            'transition: none !important' +
                            '}';
                        document.getElementsByTagName('head')[0].appendChild(style);
                    });
            };

            browser.addMockModule('disableCssAnimate', disableCssAnimate);

            var jasmineReporters = require('jasmine-reporters'),
                mkdirp = require('mkdirp');

            // returning the promise makes protractor wait for the reporter config before executing tests
            return browser.getProcessedConfig().then(function (config) {
                // you could use other properties here if you want, such as platform and version
                var browserName = config.capabilities.browserName;
                var directory = args.params.resultsDir + path.sep + browserName;
                mkdirp(directory, function (err) {
                    if (err) {
                        throw new Error('Could not create directory ' + directory);
                    }
                });

                var junitReporter = new jasmineReporters.JUnitXmlReporter({
                    consolidateAll: false, // need to work with sharding
                    savePath: directory,
                    filePrefix: 'results'
                });
                jasmine.getEnv().addReporter(junitReporter);
                browser.driver.manage().window().maximize();
                browser.manage().timeouts().setScriptTimeout(allScriptsTimeout);
            });
        },
        onCleanUp: function () {
        },
        beforeLaunch: function () {
        },
        afterLaunch: function () {
            var resultsBaseDir = args.params.resultsDir + path.sep;
            grunt.file.expand({filter: 'isDirectory', cwd: resultsBaseDir}, '*').forEach(function (dir) {

                var mergedAndUpdatedContent = '<?xml version="1.0"?>\n<testsuites>\n';
                var errors = 0;
                var tests = 0;
                var failures = 0;
                var time = 0;

                var testcases = '';
                //
                grunt.file.expand(resultsBaseDir + dir + '/*').forEach(function (file) {
                    var content = grunt.file.read(file);
                    var testsuites = content.match(/\<testsuite\s.*\>/g);

                    for (var i = 0; i < testsuites.length; i++) {
                        var match = /time="(.*)".*errors="(\d*)".*tests="(\d*)".*failures="(\d*)"/g.exec(testsuites[i]);
                        time = time + Number(match[1]);
                        errors = errors + Number(match[2]);
                        tests = tests + Number(match[3]);
                        failures = failures + Number(match[4]);
                    }

                    content = content.replace(/\<\?xml.+\?\>/gm, '');
                    content = content.replace(/\<testsuites>/gm, '');
                    content = content.replace(/\<testsuite.*>/gm, '');
                    content = content.replace(/\<\/testsuite>/gm, '');
                    content = content.replace(/\<\/testsuites>/gm, '');
                    testcases = testcases.concat(content);
                });

                var testsuite = '<testsuite ' +
                    'name="' + dir + '" ' +
                    'package="protractor" ' +
                    'tests="' + tests + '" ' +
                    'errors="' + errors + '" ' +
                    'failures="' + failures + '" ' +
                    'time="' + time + '">';
                mergedAndUpdatedContent = mergedAndUpdatedContent.concat(testsuite);
                mergedAndUpdatedContent = mergedAndUpdatedContent.concat(testcases);
                mergedAndUpdatedContent = mergedAndUpdatedContent.concat('</testsuite>');
                mergedAndUpdatedContent = mergedAndUpdatedContent.concat('</testsuites>');
                mergedAndUpdatedContent = mergedAndUpdatedContent.replace(/^\s*[\r\n]/gm, '');
                grunt.file.write(resultsBaseDir + '/protractor-' + dir + '.xml', mergedAndUpdatedContent);
            });
        },
        jasmineNodeOpts: {
            isVerbose: true,
            showColors: true,
            includeStackTrace: true,
            defaultTimeoutInterval: 40000
        }
    };

})();