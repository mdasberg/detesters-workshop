package nl.detesters.workshop.api;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Helper class for XSRF.
 */
@Named
@ApplicationScoped
public class XSRFUtil {
    private static final String XSRF_TOKEN_HEADER = "X-XSRF-TOKEN";
    private static final String XSRF_TOKEN_COOKIE = "XSRF-TOKEN";
    private static final int COOKIE_LENGTH = 20;
    private static final char[] VALID_CHARACTERS = "ABCDEFGHIJKLMNOPRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
    private static final Random RANDOM = new SecureRandom();

    /**
     * Set the xsrf cookie.
     * @param response The {@link HttpServletResponse}.
     */
    public void setXsrfCookie(final HttpServletResponse response) {
        final Cookie xsrfCookie = new Cookie(XSRF_TOKEN_COOKIE, randomToken());
        xsrfCookie.setSecure(Boolean.getBoolean("COOKIE_SECURE"));
        xsrfCookie.setPath("/");
        response.addCookie(xsrfCookie);
    }

    /**
     * Unset the xsrf cookie.
     * @param response The {@link HttpServletResponse}.
     */
    public void unsetXsrfCookie(final HttpServletResponse response) {
        final Cookie xsrfCookie = new Cookie(XSRF_TOKEN_COOKIE, null);
        xsrfCookie.setPath("/");
        xsrfCookie.setMaxAge(0); // remove the cookie
        response.addCookie(xsrfCookie);
    }

    /**
     * Determines whether the values of #XSRF_TOKEN_COOKIE and #XSRF_TOKEN_HEADER header match to identify if the request
     * is forged.
     * @param request the request.
     * @return <code>true</code> if forged, else <code>false</code>.
     */
    public boolean isForged(final HttpServletRequest request) {
        final String header = getXSRFHeader(request);
        final Cookie cookie = getXSRFCookie(request);
        return !(header != null && cookie != null && header.equals(cookie.getValue()));
    }

    /**
     * Gets the XSRF header.
     * @param request The HttpServletRequest.
     * @return token The token from the XSRF header.
     */
    public String getXSRFHeader(final HttpServletRequest request) {
        return request.getHeader(XSRF_TOKEN_HEADER);
    }

    /**
     * Gets the XSRF Cookie or null.
     * Note: It is possible that multiple cookies with the same name (on different (sub)domains) exist.
     * In this case the first will be use.
     * @param request The HttpServletRequest.
     * @return cookie The Cookie object.
     */
    public Cookie getXSRFCookie(final HttpServletRequest request) {
        final Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (final Cookie cookie : cookies) {
                final String cookieName = cookie.getName();
                if (cookieName != null && cookieName.equals(XSRF_TOKEN_COOKIE)) {
                    return cookie;
                }
            }
        }
        return null;
    }

    /**
     * Gets a  random token that can be used for XSRF cookie value.
     * @return token The seemingly useful token.
     */
    private String randomToken() {
        char[] buf = new char[COOKIE_LENGTH];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = VALID_CHARACTERS[RANDOM.nextInt(VALID_CHARACTERS.length)];
        }
        return new String(buf);
    }
}
