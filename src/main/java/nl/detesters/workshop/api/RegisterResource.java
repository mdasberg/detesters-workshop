package nl.detesters.workshop.api;

import com.mongodb.DuplicateKeyException;
import nl.detesters.workshop.config.Config;
import nl.detesters.workshop.domain.Oops;
import nl.detesters.workshop.domain.Todo;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.*;

/** API Endpoint for {@link Todo}s. */
@Path("todos")
@Produces(APPLICATION_JSON)
public class RegisterResource {

    @Inject
    private Config config;

    @GET
    public Response todos() {
        final Query<Todo> query = config.getDatastore().createQuery(Todo.class);
        final List<Todo> todos = query.asList();
        return ok(todos).build();
    }

    @GET
    @Path("{description}")
    public Response todo(@PathParam("description") final String description) throws NotFoundException {
        Response response;
        final Query<Todo> query = config.getDatastore().createQuery(Todo.class);
        final Todo todo = query.filter("description", description).get();
        if (todo != null) {
            response = ok(todo).build();
        } else {
            response = status(Status.CONFLICT).entity(new Oops("No todo with the given description exists")).build();
        }
        return response;
    }

    @POST
    @Consumes(APPLICATION_JSON)
    public Response register(final @Valid Todo todo) {
        Response response = ok().build();
        try {
            config.getDatastore().save(todo);
        } catch (DuplicateKeyException e) {
            response = status(Status.CONFLICT).entity(new Oops("A todo with the given description already exists")).build();
        } catch (Exception e) {
            response = serverError().build();
        }
        return response;
    }

    @PUT
    @Consumes(APPLICATION_JSON)
    public Response update(final @Valid Todo todo) {
        Response response = ok().build();
        try {
            final Datastore datastore = config.getDatastore();
            final Query<Todo> query = datastore.createQuery(Todo.class);
            final Todo found = query.filter("description", todo.getDescription()).get();
            final UpdateOperations<Todo> ops = datastore.createUpdateOperations(Todo.class).set("completed", todo.isCompleted());
            datastore.update(found, ops);
        } catch (DuplicateKeyException e) {
            response = status(Status.CONFLICT).entity(new Oops("A todo with the given description already exists")).build();
        } catch (Exception e) {
            response = serverError().build();
        }
        return response;
    }

    @DELETE
    @Path("{description}")
    public Response archive(@PathParam("description") final String description) throws NotFoundException {
        Response response;
        final Datastore datastore = config.getDatastore();
        final Query<Todo> query = datastore.createQuery(Todo.class);
        final Todo todo = query.filter("description", description).get();
        datastore.delete(todo);
        if (todo != null) {
            response = ok().build();
        } else {
            response = status(Status.CONFLICT).entity(new Oops("No todo with the given description exists")).build();
        }
        return response;
    }
}
