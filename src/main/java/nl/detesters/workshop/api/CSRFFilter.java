package nl.detesters.workshop.api;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.unmodifiableSet;

/**
 * CSRF Filter.
 * This filter adds a XSRF_TOKEN cookie to the response if the method is not POST, PUT or DELETE.
 * When the request is a POST, PUT or DELETE, it will check if the value of the XSRF-TOKEN cookie equals
 * the X-XSRF-TOKEN.
 * Afterwards the cookie gets a new token.
 * @see <a href="https://docs.angularjs.org/api/ng/service/$http">https://docs.angularjs.org/api/ng/service/$http</a>
 * @see <a href="http://en.wikipedia.org/wiki/Cross-site_request_forgery">http://en.wikipedia.org/wiki/Cross-site_request_forgery</a>
 */
@WebFilter(filterName = "CSRFFilter", urlPatterns = "*")
public class CSRFFilter implements Filter {
    private static final Set<String> METHODS_TO_CHECK;

    @Inject
    private XSRFUtil util;

    static {
        final Set<String> mtc = new HashSet<String>();
        mtc.add("POST");
        mtc.add("PUT");
        mtc.add("DELETE");
        METHODS_TO_CHECK = unmodifiableSet(mtc);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing to do here!
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        boolean proceed = true;
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (METHODS_TO_CHECK.contains(request.getMethod())) {
            proceed = !util.isForged(request);
        }

        if (proceed) {
            util.setXsrfCookie(response);
            chain.doFilter(request, response);
        } else {
            util.unsetXsrfCookie(response);
            response.setStatus(403);
        }
    }



    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        // nothing to do here!
    }
}