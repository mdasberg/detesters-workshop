package nl.detesters.workshop.domain;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;

/**
 * The To do  domain class which represents a to do.
 */
@Entity(value = "todos", noClassnameStored = true)
public class Todo {
    @Id
    private ObjectId id;
    @Indexed(unique = true)
    @Property("description")
    private String description;
    @Property("completed")
    private boolean completed;

    /**
     * Default constructor.
     */
    public Todo() {
    }

    /**
     * Constructor.
     * @param description The description.
     */
    public Todo(final String description) {
        this.description = description;
    }

    /**
     * Gets the description.
     * @return description The description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * @param description The description.
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Indicates if the to do has been completed.
     * @return <code>true</code> if completed
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * Sets the indicator completed.
     * @param completed The indicator.
     */
    public void setCompleted(final boolean completed) {
        this.completed = completed;
    }
}
