
package nl.detesters.workshop.domain;

/**
 * Oops.
 */
public class Oops {

    private String message;

    /**
     * Constructor.
     * @param message The message.
     */
    public Oops(String message) {
        this.message = message;
    }

    /**
     * Gets the message.
     * @return message The message.
     */
    public String getMessage() {
        return message;
    }
}