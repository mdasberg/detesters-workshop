package nl.detesters.workshop.config;

import com.mongodb.MongoClient;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

/** Application configuration. */
@Named
@ApplicationScoped
public class Config {
    private Datastore datastore;
    private Session session;

    /**
     * Initialize the property configuration.
     * @throws ConfigurationException
     */
    @PostConstruct
    void init() throws ConfigurationException {
        final PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration("workshop.properties");
        propertiesConfiguration.setReloadingStrategy(new FileChangedReloadingStrategy());
        final Properties mailServerProperties = new Properties();
        mailServerProperties.put("mail.smtp.host", propertiesConfiguration.getString("mail.smtp.host"));
        mailServerProperties.put("mail.smtp.socketFactory.port", "465");
        mailServerProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        mailServerProperties.put("mail.smtp.auth", propertiesConfiguration.getString("mail.smtp.auth"));
        mailServerProperties.put("mail.smtp.port", propertiesConfiguration.getString("mail.smtp.port"));

        session = Session.getInstance(mailServerProperties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(propertiesConfiguration.getString("mail.username"), propertiesConfiguration.getString("mail.password"));
                    }
                });


        final Morphia morphia = new Morphia();

        morphia.mapPackage("nl.detesters.workshop.domain");
        datastore = morphia.createDatastore(new MongoClient(), propertiesConfiguration.getString("mongo.database"));
        datastore.ensureIndexes();
    }

    /**
     * Get a mail session.
     * @return session The mail Session.
     */
    public Session getSession() {
        return session;
    }

    /**
     * Gets the datastore.
     * @return datastore The datastore.
     */
    public Datastore getDatastore() {
        return datastore;
    }


}