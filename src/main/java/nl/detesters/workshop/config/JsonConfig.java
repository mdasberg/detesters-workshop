package nl.detesters.workshop.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/** Json {@link ContextResolver}. */
@Provider
public class JsonConfig implements ContextResolver<ObjectMapper> {
    final ObjectMapper mapper;

    /** Constructor. */
    public JsonConfig() {
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        mapper.setSerializationInclusion(Include.NON_NULL);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    /**
     * Get the {@link ObjectMapper}.
     * @param type The class.
     * @return mapper The {@link ObjectMapper}.
     */
    @Override
    public ObjectMapper getContext(Class<?> type) {
        return mapper;
    }
}