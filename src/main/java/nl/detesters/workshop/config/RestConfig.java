package nl.detesters.workshop.config;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.ApplicationPath;

/** Rest configuration. */
@ApplicationPath("api")
public class RestConfig extends ResourceConfig {
    
    /** Constructor. */
    public RestConfig() {
        packages("nl.detesters.workshop.api");
        property(ServerProperties.WADL_FEATURE_DISABLE, false);
        property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true);
        register(LoggingFilter.class);
        register(JacksonFeature.class);
    }
}