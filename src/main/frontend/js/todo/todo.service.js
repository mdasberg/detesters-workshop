(function() {
    'use strict';

    function TodoService($resource) {
        return $resource('/api/todos/:description', {

        }, {
            get: {
                params:{'description': '@description'},
                method: 'GET',
                isArray: true
            },
            query: {
                method: 'GET',
                isArray: false
            },
            add: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            },
            archive: {
                method: 'DELETE'
            }
        });
    }

    TodoService.$inject = ['$resource'];

    angular
        .module('workshop')
        .factory('todoService', TodoService);
})();