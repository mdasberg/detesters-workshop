(function () {
    'use strict';


    function TodoController(todoService, $q) {
        var vm = this;

        function fetchTodos() {
            todoService.get({}, function (todos) {
                vm.todos = todos;
            });
        }

        fetchTodos();

        vm.remaining = function () {
            var count = 0;
            angular.forEach(vm.todos, function (todo) {
                count += todo.completed ? 0 : 1;
            });
            return count;
        };


        vm.addTodo = function () {
            if(vm.description !== '') {
                todoService.add({
                    description: vm.description
                }, function () {
                    fetchTodos();
                });
                vm.description = '';
            }
        };

        vm.update = function (todo) {
            todoService.update(todo, function () {
                fetchTodos();
            });
            vm.description = '';
        };

        vm.archive = function () {
            var oldTodos = vm.todos;
            var archived = [];
            angular.forEach(oldTodos, function (todo) {
                if (todo.completed) {
                    archived.push(archive(todo));
                }
            });
            $q.all(archived).then(function() {
                fetchTodos();
            });

        };

        function archive(todo) {
            var deferred = $q.defer();
            todoService.archive(todo, function() {
                return deferred.resolve();
            }, function() {
                return deferred.reject();
            });
            return deferred.promise;
        }
    }

    TodoController.$inject = ['todoService', '$q']

    angular
        .module('workshop')
        .controller('TodoController', TodoController);
})();