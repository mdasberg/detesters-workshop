'use strict';

describe('Happy flow', function () {
    var TodoPO = require('../po/todo.po');
    var todoPo;

    beforeAll(function () {
        var basePath = require('path').resolve('.');
        var ngApimock = require(basePath + '/.tmp/mocking/protractor.mock');
        ngApimock.selectScenario(require(basePath + '/src/test/mocks/api-todos-all.json'), 'success');
        ngApimock.addMockModule();
        browser.get('/');
        todoPo = new TodoPO(by.id('todo'));
    });

    it('should show all todos', function () {
        expect(todoPo.count()).toBe('3');
    });

    it('should show remaining', function () {
        expect(todoPo.remaining()).toBe('2');
    });
});