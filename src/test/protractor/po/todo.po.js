'use strict';

/**
 * PageObject is an API for the Todo.
 * @param locator The locator of the todo
 * @constructor
 */
var TodoPO = function () {
};

TodoPO.prototype = Object.create({}, {
    remaining: {
        value: function () {
            // TODO get the element by binding and get the text
        }
    },
    count: {
        value: function () {
            // TODO get the element by binding and get the text
        }
    },
    check: {
        value: function (description) {
            // TODO get the element by id and click
        }
    }
});

module.exports = TodoPO;