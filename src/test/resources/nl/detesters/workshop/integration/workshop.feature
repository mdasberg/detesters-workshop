Feature: Todos API

  Scenario: Get todo by description
    Given I query todo by description "do something"
    When I make the rest call
    Then response should be:
      """
      {"description":"do something","completed":false}
      """

  Scenario: Get todos
    Given I query todos
    When I make the rest call
    Then response should be:
      """
      [{"description":"do something","completed":false},{"description":"do something else","completed":false}]
      """

  Scenario: Create todo without doing a get request first
    Given I create a todo with description "do something extra"
    And I did not do a get first
    When I make the rest call
    Then response should have status code "403"

  Scenario: Create todo
    Given I create a todo with description "do something extra"
    And I do a get first
    When I make the rest call
    Then response should have status code "200"