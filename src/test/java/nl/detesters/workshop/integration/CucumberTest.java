package nl.detesters.workshop.integration;

import com.mongodb.MongoClient;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import nl.detesters.workshop.domain.Todo;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"})
public class CucumberTest {

    @BeforeClass
    public static void setUp() throws Exception {
        final PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration("workshop.properties");
        final Morphia morphia = new Morphia();

        morphia.mapPackage("nl.detesters.workshop.domain");
        final Datastore datastore = morphia.createDatastore(new MongoClient(), propertiesConfiguration.getString("mongo.database"));
        datastore.getDB().dropDatabase();
        datastore.ensureIndexes();

        datastore.save(new Todo("do something"));
        datastore.save(new Todo("do something else"));

    }
}