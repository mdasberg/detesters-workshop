package nl.detesters.workshop.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/** Test class for {@link XSRFUtil}. */
// @TODO make sure junit is run with the mockito junit runner @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/runners/MockitoJUnitRunner.html
public class XSRFUtilTest {
    private static final String XSRF_COOKIE = "XSRF-TOKEN";
    private static final String XSRF_TOKEN_HEADER = "X-XSRF-TOKEN";

    // @TODO in order for the class the have all the injected dependencies, they need to be injected @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/InjectMocks.html
    private XSRFUtil util; // class under test

    // @TODO create mocks for all the objects below @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/Mock.html
    private HttpServletResponse response;
    private HttpServletRequest request;
    private Cookie cookie;

    @Test
    public void shouldSetXsrfCookie() {
        util.setXsrfCookie(response);

        // @TODO we want to verify some attribute values on the cookie so we need to capture them @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/ArgumentCaptor.html

        // @TODO verify the cookie path
        // @TODO verify the cookie value
        // @TODO verify the cookie max age
    }

    @Test
    public void shouldUnsetXsrfCookie() {
        util.unsetXsrfCookie(response);

        // @TODO we want to verify some attribute values on the cookie so we need to capture them @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/ArgumentCaptor.html

        // @TODO verify the cookie path
        // @TODO verify the cookie value
        // @TODO verify the cookie max age
    }

    @Test
    public void shouldIndicateThatTheRequestIsForgedWhenTheHeaderAndCookieDoNotMatch() {
        // @TODO in the code we check the request header value, we need to return 'token' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookies, we need to return 'new Cookie[]{cookie}' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie name, we need to return 'XSRF_COOKIE' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie value, we need to return '"another token"' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        assertTrue(util.isForged(request));
    }

    @Test
    public void shouldIndicateThatTheRequestIsForgedWhenTheHeaderIsNotPresent() {
        // @TODO in the code we check the request header value, we need to return 'null' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookies, we need to return 'new Cookie[]{cookie}' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie name, we need to return 'XSRF_COOKIE' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie value, we need to return '"another token"' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        assertTrue(util.isForged(request));
    }

    @Test
    public void shouldIndicateThatTheRequestIsForgedWhenTheCookieIsNotPresent() {
        // @TODO in the code we check the request header value, we need to return 'null' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookies, we need to return 'new Cookie[]{}' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        assertTrue(util.isForged(request));
    }

    @Test
    public void shouldIndicateThatTheRequestIsNotForgedWhenTheHeaderAndCookieMatch() {
        // @TODO in the code we check the request header value, we need to return '"token"' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookies, we need to return 'new Cookie[]{cookie}' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie name, we need to return 'XSRF_COOKIE' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie value, we need to return '"token"' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        assertFalse(util.isForged(request));
    }

    @Test
    public void shouldUseTheFirstCookieFoundWhenMoreCookiesAreAvailable() {
        // @TODO in the code we check the request cookies, we need to return 'new Cookie[]{cookie, cookie, cookie}' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie name, we need to chain the returns, first return "another_cookie", then 'XSRF_COOKIE' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check the request cookie value, we need to return '"token"' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        util.getXSRFCookie(request);

        // @TODO verify that we stop after the first match (second cookie)
    }
}