package nl.detesters.workshop.api;

import nl.detesters.workshop.util.ReflectionUtility;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.never;

/** Test class for {@link CSRFFilter}. */
// @TODO make sure junit is run with the mockito junit runner @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/runners/MockitoJUnitRunner.html
public class CSRFFilterTest {
    // @TODO in order for the class the have all the injected dependencies, they need to be injected @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/InjectMocks.html
    private CSRFFilter filter; // class under test

    // @TODO create mocks for all the objects below @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/Mock.html
    private HttpServletRequest request;
    private HttpServletResponse response;
    private FilterChain chain;
    private FilterConfig filterConfig;
    private XSRFUtil util;

    @Test
    public void shouldCheckPOSTPUTAndDELETEButNotGET() {
        final Set<String> methodsToCheck = (Set<String>) ReflectionUtility.extract(filter, "METHODS_TO_CHECK");
        assertEquals(methodsToCheck.size(), 3);
        assertEquals(methodsToCheck.stream().filter(m ->m.equals("POST")).count(), 1);
        assertEquals(methodsToCheck.stream().filter(m ->m.equals("PUT")).count(), 1);
        assertEquals(methodsToCheck.stream().filter(m ->m.equals("DELETE")).count(), 1);
        assertEquals(methodsToCheck.stream().filter(m ->m.equals("GET")).count(), 0);
    }

    @Test
    public void shouldNotCheckForXsrfWhenMethodIsGET() throws IOException, ServletException {
        // @TODO in the code we check if the request is GET, we need to return 'GET' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        filter.doFilter(request, response, chain);

        // @TODO verify that we never checked if the request was forged
    }

    @Test
    public void shouldCheckForXsrfWhenMethodIsPOST() {
        checkIfRequestWasForged("POST");
    }

    @Test
    public void shouldCheckForXsrfWhenMethodIsPUT() {
        checkIfRequestWasForged("PUT");
    }

    @Test
    public void shouldCheckForXsrfWhenMethodIsDELETE() {
        checkIfRequestWasForged("DELETE");
    }

    @Test
    public void shouldSetTheXsrfCookieAndContinueWhenTheRequestWasNotForged() throws IOException, ServletException {
        // @TODO in the code we check if the request is POST, we need to return 'POST' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check if the request is forged, we need to return 'false' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        filter.doFilter(request, response, chain);

        // @TODO verify that we set the cookie
        // @TODO verify that we did not unset the cookie
    }

    @Test
    public void shouldUnsetTheXsrfCookieAndReturn403WhenTheRequestWasForged() throws IOException, ServletException {
        // @TODO in the code we check if the request is POST, we need to return 'POST' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)
        // @TODO in the code we check if the request is forged, we need to return 'true' @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        filter.doFilter(request, response, chain);

        // @TODO verify that we did not set the cookie
        // @TODO verify that we did unset the cookie
    }


    @Test
    public void shouldDoNothing() throws ServletException {
        filter.init(filterConfig);
        filter.destroy();
    }
    private void checkIfRequestWasForged(final String method) {
        // @TODO in the code we check if the request is post put or delete, we need to return the correct value @see http://docs.mockito.googlecode.com/hg/1.9.5/org/mockito/BDDMockito.BDDStubber.html#given(T)

        try {
            filter.doFilter(request, response, chain);
        } catch (ServletException | IOException e) {
            fail();
        }

        // @TODO verify that we checked if the request was forged
    }
}