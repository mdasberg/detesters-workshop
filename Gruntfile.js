'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    var serveStatic = require('serve-static'),
        proxy = require('grunt-connect-proxy/lib/utils'),
        path = require('path');

    grunt.initConfig({
        config: {
            hosts: {
                runtime: 'localhost'
            },
            paths: {
                tmp: '.tmp',
                base: path.resolve(),
                src: 'src/main/frontend',
                test: 'src/test',
                results: 'results',
                instrumented: 'instrumented',
                config: 'config',
                build: 'src/main/webapp'
            }
        },
        clean: {
            files: [
                '<%=config.paths.tmp%>',
                '<%=config.paths.results%>',
                '<%=config.paths.instrumented%>'
            ]
        },
        instrument: {
            files: '<%=config.paths.src%>/**/*.js',
            options: {
                basePath: '<%=config.paths.instrumented%>',
                lazy: false
            }
        },
        portPick: {
            options: {
                port: 9000,
                extra: 1
            },
            runtime: {
                targets: [
                    'connect.options.port'
                ]
            },
            connectTest: {
                targets: [
                    'connect.test.options.port'
                ]
            },
            protractor: {
                targets: [
                    'protractor_coverage.options.collectorPort'
                ]
            }
        },
        connect: {
            options: {
                port: 0,
                hostname: '0.0.0.0'
            },
            proxies: [
                {
                    context: '/api',
                    host: 'localhost',
                    port: 8080,
                    https: false,
                    changeOrigin: true
                }
            ],
            runtime: {
                options: {
                    open: {
                        target: 'http://<%= config.hosts.runtime%>:<%= connect.options.port%>'
                    },
                    middleware: function (connect) {
                        var config = grunt.config.get('config');

                        return [
                            proxy.proxyRequest,
                            connect().use('/node_modules', serveStatic('node_modules')),
                            connect().use('/mocking', serveStatic(config.paths.tmp + '/mocking')),
                            connect().use('/', serveStatic(config.paths.src))
                        ];
                    }
                }
            },
            test: {
                options: {
                    port: 0,
                    open: false,
                    middleware: function (connect) {
                        var config = grunt.config.get('config');
                        return [
                            proxy.proxyRequest,
                            connect().use('/node_modules', serveStatic('node_modules')),
                            connect().use('/mocking', serveStatic(config.paths.tmp + '/mocking')),
                            connect().use('/js', serveStatic(config.paths.instrumented + path.sep + config.paths.src + path.sep + 'js')),
                            connect().use('/', serveStatic(config.paths.src))
                        ];
                    }
                }
            }
        },
        ngApimock: {
            options: {
                defaultOutputDir: '<%= config.paths.tmp %>/mocking',
            },
            mock: {
                src: '<%=config.paths.test %>/mocks',
                moduleName: 'workshop',
                dependencies: {
                    angular: '/node_modules/angular/angular.js'
                }
            }
        },
        protractor_coverage: {
            options: {
                keepAlive: true,
                noColor: false,
                collectorPort: 0,
                coverageDir: '<%=config.paths.results%>/protractor-coverage',
                args: {
                    seleniumAddress: 'http://localhost:4444/wd/hub',
                    params: {
                        resultsDir: '<%=config.paths.results%>/protractor',
                        testDir: '<%=config.paths.test%>/protractor'
                    },
                    baseUrl: 'http://localhost:<%= connect.test.options.port %>',
                    specs: ['<%=config.paths.test%>/protractor/**/*.spec.js']
                },
                configFile: 'protractor.conf.js'
            },
            all: {}
        },
        makeReport: {
            src: '<%=config.paths.results%>/protractor-coverage/*.json',
            options: {
                type: 'lcov',
                dir: '<%=config.paths.results%>/protractor-coverage',
                print: 'detail'
            }
        },
        watch: {
            js: {
                files: ['<%=config.paths.src%>/{,*/}*.js']
            },
            html: {
                files: ['<%=config.paths.src%>/partials/{,*/}*.html'],
                tasks: ['ngtemplates']
            }
        }
    });


    grunt.registerTask('serve', 'Serve the app using the distribution .', [
        'prepare',
        'portPick',
        'configureProxies',
        'connect:runtime',
        'watch'
    ]);

    grunt.registerTask('prepare', 'Prepare the build with all the necessary stuff.', [
        'clean',
        'portPick',
        'ngApimock'
    ]);

    grunt.registerTask('test', 'Execute tests.', [
        'force:on',
        'instrument',
        'connect:test',
        'protractor_coverage',
        'makeReport',
        'force:reset'
    ]);

    grunt.registerTask('default', 'Default task', function (suite) {
        grunt.task.run([
            'prepare',
            'test'
        ]);
    });
};
